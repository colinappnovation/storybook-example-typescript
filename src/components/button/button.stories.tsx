import { storiesOf } from '@storybook/react';
import * as React from 'react';
import Button from "./Button";

storiesOf("Button", module)
    .add("with text", () => (
        <Button>Hello Button</Button>
    ))
    .add("with some emoji", () => (
        <Button>😀 😎 👍 💯</Button>
    ))
    .add("inaccessible", () => (
        <Button>
            <span style={{ backgroundColor: 'red', color: 'darkRed', }}>Inaccessible button</span>
        </Button>
    ))
    .add("onclick", () => (
        <Button onClick={() => console.log('Clicked!')}>Click Me!</Button> 
    ))