import { configure, addDecorator} from '@storybook/react';
import { withA11y } from '@storybook/addon-a11y';


const req = require.context('../src/components', true, /.stories.tsx$/);

function loadStories() {
    req.keys().forEach(req);
}

configure(loadStories, module);

addDecorator(withA11y);
